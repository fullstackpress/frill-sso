<?php

namespace frill\sso\Firebase\JWT;

class SignatureInvalidException extends \UnexpectedValueException
{
}
