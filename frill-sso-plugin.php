<?php

namespace frill\sso;

/**
 * Plugin Name:     Frill SSO
 * Plugin URI:      https://gitlab.com/fullstackpress/frill-sso/-/releases
 * Description:     Enable Frill sso (single-sign-on) support in your wordpress site
 * Author:          Full Stack Press
 * Author URI:      https://gitlab.com/fullstackpress
 * Text Domain:     frill-sso
 * Version:         1.0.0
 *
 */
if (\is_readable(__DIR__ . '/vendor/autoload.php')) {
    require __DIR__ . '/vendor/autoload.php';
}
require 'includes/Settings.php';
require 'includes/RedirectHandler.php';
new Settings();
add_action('plugins_loaded', function () {
    new RedirectHandler();
});
